# Infoli_c code

This repository includes the assortment of C versions of the Inferior Olive model using an extended HH representation including gap junction fuctionality as described in : http://journals.plos.org/ploscompbiol/article/file?id=10.1371/journal.pcbi.1002814&type=printable

Currently, this includes 2 versions: 

- Original C port of the Matlab model implementing nearest neighbour connectivity and hardcode fwd-euler ODE solvers. Ported by S.Isaza (8-way)

- A modified version of the previous nearest neighbour implementation modified to include full programmable connectivity between cells
using connectivity matrix (InfoliGenericConnectivity). Modified by G.Chatzikonstantis

Codes are developed by the NCL, Neurasmus B.V. and ICCS (NTUA) as part of the BrainFrame theme of the lab.



